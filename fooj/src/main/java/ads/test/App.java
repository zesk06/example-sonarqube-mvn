package ads.test;
import java.util.logging.Logger;
/**
 * An App
 */
public class App 
{
    private static final Logger LOGGER = Logger.getLogger("FOOJ");
    private static final String NAME = "my name is slim shady";
    
    public static void main( String[] args )
    {
        LOGGER.info( "Hello World!" );
    }

    public String getName(){
      return NAME;
    }
}
